import { ConvertPropertyBindingResult } from '@angular/compiler/src/compiler_util/expression_converter';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/domain/product';
import { ProductosService } from 'src/app/services/productos.service';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})
export class NewProductComponent implements OnInit {

  product: Product = new Product()

  constructor(private router: Router,
    private productService: ProductosService) { }

  ngOnInit(): void {
  }

  goListProduct() {
    this.router.navigate(['products/list'])
  }

  save() {
    this.productService.saveProduct(this.product)
    this.product = new Product()
  }

}
